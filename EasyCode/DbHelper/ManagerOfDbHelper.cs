﻿using EasyCode.DbHelper.MySql;
using EasyCode.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCode.DbHelper
{
    public class ManagerOfDbHelper
    {
        //策略集合
        static Dictionary<DataSourceTypeEnum, IDbHelper> dic = new Dictionary<DataSourceTypeEnum, IDbHelper>();
        static ManagerOfDbHelper()
        {
            dic.Add(DataSourceTypeEnum.MySql, new MySqlHelper());
        }

        public static IDbHelper GetDbHelper(DataSourceTypeEnum dataSourceTypeEnum)
        {
            return dic[dataSourceTypeEnum];
        }
    }
}
