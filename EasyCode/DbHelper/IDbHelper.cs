﻿using EasyCode.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCode.DbHelper
{
    public interface IDbHelper
    {
        DataTable GetDatabases(DataSourceEntity dataSourceEntity);

        DataTable GetTables(DataSourceEntity dataSourceEntity);

        DataTable GetFilds(DataSourceEntity dataSourceEntity, string tableName);
    }
}
