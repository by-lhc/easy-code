﻿using EasyCode.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCode.Entity
{
    public class DataSourceEntity
    {
        public DataSourceTypeEnum DbType { get; set; }
        public string DbName { get; set; }
        public string IP { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
