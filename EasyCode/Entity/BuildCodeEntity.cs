﻿using EasyCode.Entity;
using EasyCode.FunServicer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EasyCode.Entity
{
    public class BuildCodeEntity
    {
        public string TableName { get; set; }
        public string TableCamelName { get; set; }
        public string TableCapitalizedCamelName { get; set; }
        public List<ColumnEntity> ColumnEntityList { get; set; }
        public List<ColumnEntity> PrimaryKeyColumnEntityList { get; set; }
        public List<ColumnEntity> NoPrimaryKeyColumnEntityList { get; set; }

        public BuildCodeEntity(string tableName, string tableCamelName, string tableCapitalizedCamelName,
            List<ColumnEntity> columnEntityList, List<ColumnEntity> primaryKeyColumnEntityList, List<ColumnEntity> noPrimaryKeyColumnEntityList)
        {
            this.TableName = tableName;
            this.TableCamelName = tableCamelName;
            this.TableCapitalizedCamelName = tableCapitalizedCamelName;
            this.ColumnEntityList = columnEntityList;
            this.PrimaryKeyColumnEntityList = primaryKeyColumnEntityList;
            this.NoPrimaryKeyColumnEntityList = noPrimaryKeyColumnEntityList;
        }

        /// <summary>
        /// 模型代理方法 - 供模板使用
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetAppSettings(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

    }
}