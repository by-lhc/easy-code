﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasyCode.Entity
{
    public class ColumnEntity
    {
        /// <summary>
        /// 字段类型
        /// </summary>
        public string ColumnDataType { get; set; }
        /// <summary>
        /// 字段名称
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 字段名称-驼峰模式
        /// </summary>
        public string ColumnCamelName { get; set; }
        /// <summary>
        /// 字段名称-首字母大写模式-驼峰模式
        /// </summary>
        public string ColumnCapitalizedCamelName { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string ColumnDescription { get; set; }
        /// <summary>
        /// 是否主键
        /// </summary>
        public bool PrimaryKey { get; set; }

    }
}
