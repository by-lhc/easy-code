﻿using EasyCode.FunServicer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyCode.FunServicer
{
    public static class DataSourceXmlServicer
    {
        /// <summary>
        /// 数据源的文件名称
        /// </summary>
        readonly public static string DataSourceFileName = "DataSource.xml";

        /// <summary>
        /// 数据源的DataTable结构
        /// </summary>
        /// <returns></returns>
        public static DataTable GetDataSourceDataTable()
        {
            DataTable dataTable = new DataTable("DataSource");
            dataTable.Columns.Add("DbType");
            dataTable.Columns.Add("DbName");
            dataTable.Columns.Add("IP");
            dataTable.Columns.Add("Port");
            dataTable.Columns.Add("UserName");
            dataTable.Columns.Add("Password");
            return dataTable;
        }

        public static DataTable GetDataSourceFromXml()
        {
            DataTable dataSourceDataTable = GetDataSourceDataTable();
            string filePath = Path.Combine(PathServicer.DataDirectory, DataSourceFileName);
            if (File.Exists(filePath))
            {
                dataSourceDataTable.ReadXml(filePath);
            }
            return dataSourceDataTable;
        }

        public static void SetDataSourceToXml(DataTable dataTable)
        {
            string folderPath = PathServicer.DataDirectory;
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            dataTable.WriteXml(Path.Combine(folderPath, DataSourceFileName));
        }

    }
}
