﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCode.FunServicer
{
    public static class PathServicer
    {
        /// <summary>
        /// 获取程序目录
        /// </summary>
        public static string BaseDirectory
        {
            get
            {
                return System.AppDomain.CurrentDomain.BaseDirectory;
            }
        }
        /// <summary>
        /// 获取模板目录
        /// </summary>
        public static string TemplatesDirectory
        {
            get
            {
                return Combine(BaseDirectory, "Templates");
            }
        }
        /// <summary>
        /// 获取用户数据目录
        /// </summary>
        public static string DataDirectory
        {
            get
            {
                return Combine(BaseDirectory, "Data");
            }
        }

        /// <summary>
        /// 组合路径
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static string Combine(params string[] paths)
        {
            return Path.Combine(paths);
        }

        /// <summary>
        /// 根据目录得到文件列表 - 递归
        /// </summary>
        /// <param name="fileDir"></param>
        /// <returns></returns>
        public static List<string> GetFiles(string fileDir)
        {
            List<string> list = new List<string>();

            if (!Directory.Exists(fileDir))
            {
                return list;
            }

            DirectoryInfo dirInfo = new DirectoryInfo(fileDir);
            if (dirInfo.GetFiles() != null)
            {
                foreach (FileInfo fileInfo in dirInfo.GetFiles())
                {
                    list.Add(fileInfo.FullName);
                }
            }
            if (dirInfo.GetDirectories() != null)
            {
                foreach (DirectoryInfo subDirInfo in dirInfo.GetDirectories())
                {
                    list.AddRange(GetFiles(subDirInfo.FullName));
                }
            }

            return list;
        }

        /// <summary>
        /// 根据目录得到模板列表
        /// </summary>
        /// <param name="fileDir"></param>
        /// <returns></returns>
        public static List<string> GetFilesOfTemplates()
        {
            List<string> fileList = GetFiles(TemplatesDirectory);
            return fileList.FindAll(file =>
            {
                string fileExtension = Path.GetExtension(file);
                if (fileExtension.ToLower().Trim() == ".template")
                {
                    return true;
                }
                return false;
            });
        }

    }
}
