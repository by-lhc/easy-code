﻿using EasyCode.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCode.FunServicer
{
    public class FieldMappingServicer
    {
        static Dictionary<DataSourceTypeEnum, Dictionary<LanguageTypeEnum, Dictionary<string, string>>> database_language_field_value = new Dictionary<DataSourceTypeEnum, Dictionary<LanguageTypeEnum, Dictionary<string, string>>>();

        public static string GetMappingField(DataSourceTypeEnum database, LanguageTypeEnum language, string field)
        {
            string mappingField;
            try
            {
                mappingField = database_language_field_value[database][language][field];
            }
            catch
            {
                mappingField = field;
            }
            return mappingField;
        }

        static FieldMappingServicer()
        {
            MysqlMapping();
        }

        static void MysqlMapping()
        {
            // 1.1 mysql
            Dictionary<LanguageTypeEnum, Dictionary<string, string>> mysql_language_field_value = new Dictionary<LanguageTypeEnum, Dictionary<string, string>>();
            // 2.1 mysql-java
            Dictionary<string, string> mysql_java_field_value = new Dictionary<string, string>();
            mysql_java_field_value.Add("tinyint", "Short");
            mysql_java_field_value.Add("smallint", "Short");
            mysql_java_field_value.Add("mediumint", "Integer");
            mysql_java_field_value.Add("int", "Integer");
            mysql_java_field_value.Add("integer", "Integer");
            mysql_java_field_value.Add("bigint", "Long");
            mysql_java_field_value.Add("float", "Float");
            mysql_java_field_value.Add("double", "Double");
            mysql_java_field_value.Add("decimal", "BigDecimal");
            mysql_java_field_value.Add("char", "String");
            mysql_java_field_value.Add("varchar", "String");
            mysql_java_field_value.Add("text", "String");
            mysql_java_field_value.Add("date", "Date");
            mysql_java_field_value.Add("datetime", "Date");
            mysql_language_field_value.Add(LanguageTypeEnum.Java, mysql_java_field_value);
            // 2.2 mysql-cs
            Dictionary<string, string> mysql_cs_field_value = new Dictionary<string, string>();
            mysql_cs_field_value.Add("tinyint", "short");
            mysql_cs_field_value.Add("smallint", "short");
            mysql_cs_field_value.Add("mediumint", "int");
            mysql_cs_field_value.Add("int", "int");
            mysql_cs_field_value.Add("integer", "int");
            mysql_cs_field_value.Add("bigint", "long");
            mysql_cs_field_value.Add("float", "float");
            mysql_cs_field_value.Add("double", "double");
            mysql_cs_field_value.Add("decimal", "decimal");
            mysql_cs_field_value.Add("char", "string");
            mysql_cs_field_value.Add("varchar", "string");
            mysql_cs_field_value.Add("text", "string");
            mysql_cs_field_value.Add("date", "DateTime");
            mysql_cs_field_value.Add("datetime", "DateTime");
            mysql_language_field_value.Add(LanguageTypeEnum.Cs, mysql_cs_field_value);
            // 3.1 add
            database_language_field_value.Add(DataSourceTypeEnum.MySql, mysql_language_field_value);
        }

    }
}
