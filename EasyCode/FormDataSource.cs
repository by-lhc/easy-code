﻿using EasyCode.FunServicer;
using EasyCode.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyCode.Enums;

namespace EasyCode
{
    public partial class FormDataSource : Form
    {
        //被选中的数据源
        public DataSourceEntity SelectedDataSourceEntity { get; set; }
        //数据源列表
        private DataTable dataSourceDataTable;

        public FormDataSource()
        {
            InitializeComponent();
        }

        private void FormDataSource_Load(object sender, EventArgs e)
        {
            //获取配置的数据源列表
            this.dataSourceDataTable = DataSourceXmlServicer.GetDataSourceFromXml();
            this.dataGridView1.DataSource = this.dataSourceDataTable;
            //隐藏账密列
            this.dataGridView1.Columns[4].Visible = false;
            this.dataGridView1.Columns[5].Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormDataSourceAdd formDataSourceAdd = new FormDataSourceAdd();
            if (formDataSourceAdd.ShowDialog() == DialogResult.OK)
            {
                DataSourceEntity dataSourceEntity = formDataSourceAdd.DataSourceEntity;
                this.dataSourceDataTable.Rows.Add(new object[] {
                    dataSourceEntity.DbType.ToString(),
                    dataSourceEntity.DbName,
                    dataSourceEntity.IP,
                    dataSourceEntity.Port,
                    dataSourceEntity.UserName, 
                    dataSourceEntity.Password 
                });
                DataSourceXmlServicer.SetDataSourceToXml(this.dataSourceDataTable);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows != null && this.dataGridView1.SelectedRows.Count > 0)
            {
                this.dataGridView1.Rows.Remove(this.dataGridView1.SelectedRows[0]);
                DataSourceXmlServicer.SetDataSourceToXml(this.dataSourceDataTable);
            }
            else
            {
                MessageBox.Show("请选择要删除的数据源");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows != null && this.dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];

                SelectedDataSourceEntity = new DataSourceEntity();
                this.SelectedDataSourceEntity.DbType = (DataSourceTypeEnum)Enum.Parse(typeof(DataSourceTypeEnum), row.Cells["DbType"].Value.ToString());
                this.SelectedDataSourceEntity.DbName = row.Cells["DbName"].Value.ToString();
                this.SelectedDataSourceEntity.IP = row.Cells["IP"].Value.ToString();
                this.SelectedDataSourceEntity.Port = row.Cells["Port"].Value.ToString();
                this.SelectedDataSourceEntity.UserName = row.Cells["UserName"].Value.ToString();
                this.SelectedDataSourceEntity.Password = row.Cells["Password"].Value.ToString();

                base.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("请选择要连接的数据源");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
        }

    }
}
