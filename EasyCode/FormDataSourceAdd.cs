﻿using EasyCode.DbHelper;
using EasyCode.Entity;
using EasyCode.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyCode
{
    public partial class FormDataSourceAdd : Form
    {
        public DataSourceEntity DataSourceEntity { get; set; }

        public FormDataSourceAdd()
        {
            InitializeComponent();
        }

        private void FormDataSourceAdd_Load(object sender, EventArgs e)
        {
            foreach ( DataSourceTypeEnum databaseType in Enum.GetValues(typeof(DataSourceTypeEnum)) )
            {
                this.comboBox1.Items.Add(databaseType.ToString());
            }
            this.comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dataSourceType = this.comboBox1.SelectedItem.ToString();
            DataSourceTypeEnum dataSourceTypeEnum = (DataSourceTypeEnum)Enum.Parse(typeof(DataSourceTypeEnum), dataSourceType);
            switch (dataSourceTypeEnum)
            {
                case DataSourceTypeEnum.MySql:
                    textBox2.Text = "3306";
                    break;
                case DataSourceTypeEnum.SqlServer:
                    textBox2.Text = "1433";
                    break;
                case DataSourceTypeEnum.Oracle:
                    textBox2.Text = "1521";
                    break; 
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //获取页面数据
            string dbtype = this.comboBox1.SelectedItem.ToString();
            string ip = this.textBox1.Text;
            string port = this.textBox2.Text;
            string username = this.textBox3.Text;
            string password = this.textBox4.Text;
            //赋值实体类
            this.DataSourceEntity = new DataSourceEntity();
            this.DataSourceEntity.DbType = (DataSourceTypeEnum)Enum.Parse(typeof(DataSourceTypeEnum), dbtype);
            this.DataSourceEntity.IP = ip;
            this.DataSourceEntity.Port = port;
            this.DataSourceEntity.UserName = username;
            this.DataSourceEntity.Password = password;

            if (string.IsNullOrEmpty(this.DataSourceEntity.IP)
                || string.IsNullOrEmpty(this.DataSourceEntity.Port)
                || string.IsNullOrEmpty(this.DataSourceEntity.UserName)
                || string.IsNullOrEmpty(this.DataSourceEntity.Password))
            {
                MessageBox.Show("请完整填写数据库信息");
            }
            else if (this.DataSourceEntity.DbType == DataSourceTypeEnum.SqlServer
                || this.DataSourceEntity.DbType == DataSourceTypeEnum.Oracle)
            {
                MessageBox.Show("暂不支持该数据库类型");
            }
            else
            {
                try
                {
                    DataTable dt = ManagerOfDbHelper.GetDbHelper(this.DataSourceEntity.DbType).GetDatabases(this.DataSourceEntity);
                    this.comboBox2.DataSource = dt;
                    this.comboBox2.DisplayMember = "SCHEMA_NAME";
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.comboBox2.SelectedItem == null)
            {
                MessageBox.Show("请选择一个数据库");
            }
            else {
                this.DataSourceEntity.DbName = this.comboBox2.Text;
                base.DialogResult = DialogResult.OK;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
        }
    }
}
