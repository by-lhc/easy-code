﻿using EasyCode.DbHelper;
using EasyCode.Entity;
using EasyCode.Enums;
using EasyCode.FunServicer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyCode
{
    public partial class FormEasyCode : Form
    {
        //选择的数据库
        private DataSourceEntity dataSourceEntity;

        public FormEasyCode()
        {
            InitializeComponent();
        }

        private void FormEasyCode_Load(object sender, EventArgs e)
        {
            选择数据源ToolStripMenuItem_Click(null, null);
        }

        private void 选择数据源ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDataSource formDataSource = new FormDataSource();
            if (formDataSource.ShowDialog() == DialogResult.OK)
            {
                //选择数据库
                dataSourceEntity = formDataSource.SelectedDataSourceEntity;
                //获取表信息
                DataTable table = ManagerOfDbHelper.GetDbHelper(dataSourceEntity.DbType).GetTables(dataSourceEntity);
                //根据库表信息创建树节点
                TreeNode rootNode = new TreeNode( String.Format("[{0}] {1} ({2}:{3})",
                    dataSourceEntity.DbType, dataSourceEntity.DbName, dataSourceEntity.IP, dataSourceEntity.Port) );
                rootNode.Expand();
                foreach (DataRow row in table.Rows)
                {
                    TreeNode nodeTable = new TreeNode(row["TABLE_NAME"].ToString())
                    {
                        Tag = dataSourceEntity
                    };
                    rootNode.Nodes.Add(nodeTable);
                }
                this.treeView1.Nodes.Clear();
                this.treeView1.Nodes.Add(rootNode);
            }
        }

        private void 生成代码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = this.treeView1.SelectedNode;
            //选择的是叶子节点则生成代码
            if ( selectedNode != null && (selectedNode.Nodes == null || selectedNode.Nodes.Count == 0) )
            {
                //获取模板列表
                List<string> templateList = PathServicer.GetFilesOfTemplates();
                if (templateList == null || templateList.Count == 0)
                {
                    MessageBox.Show("没有找到模板");
                    return;
                }
                //获取目标语言
                LanguageTypeEnum languageTypeEnum;
                try
                {
                    string targetLanguage = ConfigurationManager.AppSettings["TargetLanguage"].ToString();
                    languageTypeEnum = (LanguageTypeEnum)Enum.Parse(typeof(LanguageTypeEnum), targetLanguage);
                }
                catch
                {
                    MessageBox.Show("没有配置目标语言");
                    return;
                }
                //TabControl添加一个代码生成窗体
                string selectedTableName = selectedNode.Text;
                DataSourceEntity selectedDataSourceEntity = selectedNode.Tag as DataSourceEntity;
                FormBuildCode form = new FormBuildCode(selectedDataSourceEntity, selectedTableName, languageTypeEnum, templateList);
                Crownwood.Magic.Controls.TabPage tabPage = new Crownwood.Magic.Controls.TabPage(selectedTableName, form, 0);
                this.tabControl1.TabPages.Add(tabPage);
                tabPage.Selected = true;
            }
            else
            {
                MessageBox.Show("请选择要生成代码的表");
            }
        }

        private void 开发者ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("lhc");
        }

        private void tabControl1_ClosePressed(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedTab != null)
            {
                this.tabControl1.TabPages.Remove(this.tabControl1.SelectedTab);
            }
        }

    }
}
