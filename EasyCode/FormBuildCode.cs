﻿using EasyCode.DbHelper;
using EasyCode.Entity;
using EasyCode.Enums;
using EasyCode.FunServicer;
using RazorEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyCode
{
    public partial class FormBuildCode : Form
    {
        DataSourceEntity dataSourceEntity;
        string tableName;
        LanguageTypeEnum languageTypeEnum;
        List<string> templates;
        BuildCodeEntity model;

        public FormBuildCode()
        {
            InitializeComponent();
        }
        public FormBuildCode(DataSourceEntity dataSourceEntity, string tableName, LanguageTypeEnum languageTypeEnum, List<string> templates)
        {
            InitializeComponent();
            this.dataSourceEntity = dataSourceEntity;
            this.tableName = tableName;
            this.languageTypeEnum = languageTypeEnum;
            this.templates = templates;
            InitModel();
        }

        private void InitModel()
        {
            //表名 - 驼峰模式、首字母大写模式
            string tableCamelName = StringServicer.ConvertCamelName(tableName);
            string tableCapitalizedCamelName = StringServicer.FirstCharToUpper(tableCamelName);
            //获取字段信息
            DataTable fieldsTable = ManagerOfDbHelper.GetDbHelper(dataSourceEntity.DbType).GetFilds(dataSourceEntity, tableName);
            List<ColumnEntity> columnEntityList = new List<ColumnEntity>();
            List<ColumnEntity> primaryKeyColumnEntityList = new List<ColumnEntity>();
            List<ColumnEntity> noPrimaryKeyColumnEntityList = new List<ColumnEntity>();
            foreach (DataRow dr in fieldsTable.Rows)
            {
                //字段信息转实体
                ColumnEntity colum = new ColumnEntity();
                colum.ColumnDataType = FieldMappingServicer.GetMappingField(dataSourceEntity.DbType, languageTypeEnum,
                    dr["DATA_TYPE"].ToString().Trim().ToLower());
                colum.ColumnName = dr["COLUMN_NAME"].ToString().Trim();
                colum.ColumnCamelName = StringServicer.ConvertCamelName(colum.ColumnName);
                colum.ColumnCapitalizedCamelName = StringServicer.FirstCharToUpper(colum.ColumnCamelName);
                colum.ColumnDescription = dr["COLUMN_COMMENT"].ToString().Trim().Replace("\r\n", " ");
                colum.PrimaryKey = dr["COLUMN_KEY"].ToString().Trim().Equals("Y");
                //字段信息分组
                columnEntityList.Add(colum);
                if (colum.PrimaryKey)
                {
                    primaryKeyColumnEntityList.Add(colum);
                }
                else
                {
                    noPrimaryKeyColumnEntityList.Add(colum);
                }
            }
            //模型
            model = new BuildCodeEntity(tableName, tableCamelName, tableCapitalizedCamelName,
                columnEntityList, primaryKeyColumnEntityList, noPrimaryKeyColumnEntityList);
        }

        private void FormBuildCode_Load(object sender, EventArgs e)
        {
            foreach (string templateFile in templates)
            {
                string fileName = StringServicer.BuildCodeFileName(templateFile, model);
                string resultContent = StringServicer.BuildCodeContent(templateFile, model);

                //RichTextBox
                RichTextBox rtb = new System.Windows.Forms.RichTextBox();
                rtb.Dock = DockStyle.Fill;
                rtb.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
                rtb.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)134);
                rtb.Text = resultContent;
                //TabPage
                TabPage tp = new TabPage();
                tp.Text = " " + fileName + " ";
                tp.Controls.Add(rtb);
                //TabControl
                this.tabControl1.Controls.Add(tp);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("敬请期待");
        }

    }
}
