# EasyCode

#### 介绍
这是一个Windows桌面应用。

根据数据库表、代码模板，生成代码。

支持数据库表【多个主键】。


#### 安装教程

1. 下载源码编译成可执行文件 或者 直接下载可执行文件。
2. 点击根目录中后缀为“exe”的可执行文件。

#### 使用说明

1. 修改根目录下，后缀为“config”的配置文件，TargetLanguage配置修改为Java或cs（c#）。

2. 修改根目录下，“Templates”文件夹里的模板名称和内容。

   模板名称规则：
   
   ​	例如[TableName].java.template，“[TableName]”将被替换成类名称，“.template”将被去掉。
   
   模板内容规则：
   
   ​	[Razor](https://www.runoob.com/aspnet/razor-intro.html)语法规则替换模型数据。
   
   模型结构如下：
   
   ```
       public class BuildCodeEntity
       {
           //表名
           public string TableName { get; set; }
           ////表名-驼峰
           public string TableCamelName { get; set; }
           //表名-首字母大写驼峰
           public string TableCapitalizedCamelName { get; set; }
           //属性集合
           public List<ColumnEntity> ColumnEntityList { get; set; }
           //属性集合-主键
           public List<ColumnEntity> PrimaryKeyColumnEntityList { get; set; }
           //属性集合-非主键
           public List<ColumnEntity> NoPrimaryKeyColumnEntityList { get; set; }
       }
       public class ColumnEntity
       {
           /// <summary>
           /// 字段类型-根据数据库和目标语言映射
           /// </summary>
           public string ColumnDataType { get; set; }
           /// <summary>
           /// 字段名称
           /// </summary>
           public string ColumnName { get; set; }
           /// <summary>
           /// 字段名称-驼峰模式
           /// </summary>
           public string ColumnCamelName { get; set; }
           /// <summary>
           /// 字段名称-首字母大写驼峰模式
           /// </summary>
           public string ColumnCapitalizedCamelName { get; set; }
           /// <summary>
           /// 描述
           /// </summary>
           public string ColumnDescription { get; set; }
           /// <summary>
           /// 是否主键
           /// </summary>
           public bool PrimaryKey { get; set; }
       }
   ```
   
   
